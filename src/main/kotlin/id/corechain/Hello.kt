package id.corechain

import com.github.salomonbrys.kotson.get
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.mashape.unirest.http.Unirest
import spark.Spark.*
import java.io.FileInputStream
import java.util.*

fun getToken(): String? {
    val props = Properties()
    val input = FileInputStream("/etc/service.conf");
    props.load(input)
    return props.get("line.token").toString()
}

fun main(args: Array<String>) {
    port(7012)
    get("/api/bot/webhook") { req, res ->
        return@get req.queryParams("hub.challenge")
    }

    post("/api/bot/webhook") { req, res ->
        var body:String = req.body()
        var header: MutableSet<String>? = req.headers()
        val json: JsonObject
        val parser: JsonParser = JsonParser()
        print(body)
        try{
            var param ="{\"recipient\": {\"id\": \"%s\"},\"message\": {\"text\": \"%s\"}}"
            json = parser.parse(body) as JsonObject
            var textSplit= json.get("entry").get(0).get("messaging").get(0).get("message").get("text").asString.split(" ")
            var clientID =json.get("entry").get(0).get("messaging").get(0).get("sender").get("id").asString
            var type =textSplit[0]
            if(type.equals("/register")){
                if(textSplit.size==4){
                    sendMessage(String.format(param,clientID,"register with cashtag : "), header!!)
                }else{
                    sendMessage(String.format(param,clientID,"invalid parameter"), header!!)
                }
            }else if(type.equals("/saldo")){
                if(textSplit.size==1){
                    sendMessage(String.format(param,clientID,"IDR 27.000,00"), header!!)
                }else{
                    sendMessage(String.format(param,clientID,"invalid parameter"), header!!)
                }
            }else if(type.equals("/info")){
                if(textSplit.size==1){
                    sendMessage(String.format(param,clientID,"Discount 20% Starbuck"), header!!)
                }else{
                    sendMessage(String.format(param,clientID,"invalid parameter"), header!!)
                }
            }else if(type.equals("/topup")){
                if(textSplit.size==2){
                    var amount = textSplit[1].toInt()+1
                    sendMessage(String.format(param,clientID,"BCA 8160xxxxxx\\n" +
                            "Mandiri 8160xxxxxx\\n"+
                            "Maybank 8160xxxxxx\\n" +
                            "Jumlah yang harus dibayar : "+amount), header!!)
                }else{
                    sendMessage(String.format(param,clientID,"invalid parameter"), header!!)
                }
            }else if(type.equals("/send")){
                if(textSplit.size==3){
                    var amount = textSplit[1].toInt()
                    sendMessage(String.format(param,clientID,"send to "+textSplit[2]+" amount: "+amount), header!!)
                }else{
                    sendMessage(String.format(param,clientID,"invalid parameter"), header!!)
                }
            }else if(type.equals("/cashout")){
                if(textSplit.size==4){
                    var amount = textSplit[1].toInt()
                    sendMessage(String.format(param,clientID,"cash out "+textSplit[1]+" to "+textSplit[2]+textSplit[3]), header!!)
                }else{
                    sendMessage(String.format(param,clientID,"invalid parameter"), header!!)
                }
            }else if(type.equals("/infobank")){
                if(textSplit.size==1){
                    sendMessage(String.format(param,clientID,"BCA 014\\nMandiri 008\\nMaybank 016\\n"), header!!)
                }else{
                    sendMessage(String.format(param,clientID,"invalid parameter"), header!!)
                }
            }else{
                sendMessage(String.format(param,clientID,"invalid parameter"), header!!)
            }
        }catch (e:Exception){
            e.printStackTrace()
            print("API Problem")
        }
        return@post "ok"
    }
}

fun sendMessage(body:String, header:MutableSet<String>){
    var res =Unirest.post("https://graph.facebook.com/v2.6/me/messages?access_token=EAABeQj3BxE0BALkzpSG8NCBUk3lVlupOJIj1sJAxicTWlakx9p9rFgD78Hec1Y3ZBqTaHnKecP69JH7RRYEozmpDDGz8h5hhsw8mIhIDzDAZBZCbn4E53lOEwa1SRTZAgbxJs6b6LBWi6a2w37RroyhHZC2mWEFmdwVdDUZAQD2wZDZD")
            .header("Content-Type","application/json")
            .body(body).asString()
}
